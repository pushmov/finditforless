<?php
$contName = Inflector::camelize($this->params['controller']);
$actName = $this->params['action'];
$actionUrl = $contName.'/'.$actName;
$activeClass='active';
$inactiveClass='';

?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <?php echo $this->Html->charset(); ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
	
	<title>Offers | Find It For Less</title>
	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>

	<?php 
		echo $this->Html->css('bootstrap.min.css'); 
		echo $this->Html->css('style.css');
		echo $this->Html->css('styles.css');
		echo $this->Html->css('font-awesome.min.css');
		echo $this->Html->css('polaris/polaris.css'); 
	?>
	<link href="/fifl/css/themes/fifl.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><?php echo $this->Html->script('ie8-responsive-file-warning.js');?><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

</head>

<body>
		
	<div class="header navbar navbar-inverse navbar-fixed-top" role="navigation">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="header-inner">
		<!-- BEGIN LOGO -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle btn blue search-collapse" id="search-collapse"><i class="fa fa-search fa-lg white"></i></button>
      <button type="button" class="navbar-toggle btn blue search-collapse" id="menu-collapse" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
      </button>
			<a class="navbar-brand" href="index.html">
				<img src="/fifl/img/logo.png" alt="logo" class="img-responsive"/>
			</a>
    </div>
    
		<div class="collapse navbar-collapse navbar-left">
			<ul class="nav navbar-nav">
				<li><a href="index.html">Dashboard </a></li>
				<li>
					<a data-toggle="dropdown" data-hover="dropdown" data-close-others="true" class="dropdown-toggle" href=""><i class="fa fa-gbp white"></i> Offers <i class="fa fa-angle-down white"></i></a>
					<ul class="dropdown-menu">

<li><?php echo $this->Html->link('Top Offers', array(
                                    'controller' => 'offers',
                                    'action' => 'bycategory',
'plugin' => NULL,
                                    'catslug' => 'top'
                                                                   )); ?></li>
<li><?php echo $this->Html->link('Latest Offers', array(
                                    'controller' => 'offers',
                                    'action' => 'bycategory',
'plugin' => NULL,
                                    'catslug' => 'newest'
                                                                   )); ?></li>

<li><?php echo $this->Html->link('Popular Offers', array(
                                    'controller' => 'offers',
                                    'action' => 'bycategory',
'plugin' => NULL,
                                    'catslug' => 'popular'

                                                                   )); ?></li>								

<li class="divider"></li>
 <li role="presentation" class="dropdown-header">Categories</li>
<?php foreach($menucats as $menucat): ?>
<li><?php echo $this->Html->link(''.$menucat['Category']['name'].'', array(
                                    'controller' => 'offers',
                                    'action' => 'bycategory',
'plugin' => NULL,
                                    'catslug' => $menucat['Category']['slug']
                                                                   )); ?></li>

 <?php endforeach; ?> 

					</ul>
				</li>

        <?php if ($this->UserAuth->isAdmin()) { ?>
				<li>
					<a data-toggle="dropdown" data-hover="dropdown" data-close-others="true" class="dropdown-toggle" href=""><i class="fa fa-cogs"></i> Admin <i class="fa fa-angle-down"></i></a>
					<ul class="dropdown-menu"><?php  
					//Searches Menu
					if($this->UserAuth->HP('Searches', 'index')) { ?>
          <li  class="dropdown-submenu <?php if ($actionUrl=='Searches/index'||$actionUrl=='UserGroups/index') { ?> active<?php } ?>">
						<a href="javascript:;">
							<i class="fa fa-search"></i>
							<span class="title">Searches</span>
							<?php if ($actionUrl=='UserGroups/addGroup'||$actionUrl=='UserGroups/index') { ?><span class="selected"></span><?php } ?>
							<span class="arrow <?php if ($actionUrl=='UserGroups/addGroup'||$actionUrl=='UserGroups/index') { ?>open<?php } ?>"></span>
						</a>
						<ul class="dropdown-menu">
						<?php
							if($this->UserAuth->HP('Searches', 'index')) {
								echo "<li class='".(($actionUrl=='Searches/index') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('View Searches'), array('controller'=>'Searches', 'action'=>'index', 'plugin'=>null))."</li>";
							}
							if($this->UserAuth->HP('UserGroups', 'index')) {
								echo "<li class='".(($actionUrl=='UserGroups/index') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('All Groups'), array('controller'=>'UserGroups', 'action'=>'index', 'plugin'=>'usermgmt'))."</li>";
							}
						?>
						</ul>
					</li>
					<?php } ?>
          <?php  //Transactions Menu
					if($this->UserAuth->HP('UserGroups', 'addGroup')) { ?>
						<li class="dropdown-submenu <?php if ($actionUrl=='UserGroups/addGroup'||$actionUrl=='UserGroups/index') { ?>active <?php } ?>">
							<a href="javascript:;">
								<i class="fa fa-truck"></i>
								<span class="title">Transactions</span>
								<?php if ($actionUrl=='UserGroups/addGroup'||$actionUrl=='UserGroups/index') { ?><span class="selected"></span><?php } ?>
								<span class="arrow <?php if ($actionUrl=='UserGroups/addGroup'||$actionUrl=='UserGroups/index') { ?>open<?php } ?>"></span>
							</a>
							<ul class="dropdown-menu">
							<?php
					
					if($this->UserAuth->HP('UserGroups', 'addGroup')) {
								echo "<li class='".(($actionUrl=='UserGroups/addGroup') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Add Group'), array('controller'=>'UserGroups', 'action'=>'addGroup', 'plugin'=>'usermgmt'))."</li>";
							}
							if($this->UserAuth->HP('UserGroups', 'index')) {
								echo "<li class='".(($actionUrl=='UserGroups/index') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('All Groups'), array('controller'=>'UserGroups', 'action'=>'index', 'plugin'=>'usermgmt'))."</li>";
							}  ?>
                            </ul></li>
                            <?php
			  
			  
			   }?>
                     <?php
					   
					   //Categories Menu
					   
					    if($this->UserAuth->HP('Categories', 'add')) { ?>
                    
                 <li class="dropdown-submenu <?php if ($actionUrl=='Categories/add'||$actionUrl=='Categories/index') { ?> active <?php } ?>">
					<a href="javascript:;">
					<i class="fa fa-folder-open-o"></i>
					<span class="title">
						Categories
					</span>
					<?php if ($actionUrl=='Categories/add'||$actionUrl=='Categories/index') { ?><span class="selected">
					</span><?php } ?>
					<span class="arrow <?php if ($actionUrl=='Categories/add'||$actionUrl=='Categories/index') { ?>open<?php } ?>">
					</span></a>
					<ul class="dropdown-menu">
                <?php
            
				
							if($this->UserAuth->HP('Categories', 'add')) {
								echo "<li class='".(($actionUrl=='Categories/add') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Add Category'), array('controller'=>'Categories', 'action'=>'add', 'plugin'=>null))."</li>";
							}
							
							if($this->UserAuth->HP('Users', 'index')) {
								echo "<li class='".(($actionUrl=='Categories/index') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Edit Categories'), array('controller'=>'Categories', 'action'=>'index','plugin'=>null))."</li>";
							}	 ?>
                            
                            </ul>
				</li>
                <?php					
					}
					?> 
					 <?php 	
				
				//Users Menu
				
				
				if($this->UserAuth->HP('Users', 'addUser')) { ?>
				<li class="dropdown-submenu <?php if ($actionUrl=='Users/addUser'||$actionUrl=='Users/addMultipleUsers'||$actionUrl=='Users/index'||$actionUrl=='Users/online') { ?> active <?php } ?>">
					<a href="javascript:;">
					<i class="fa fa-user"></i>
					<span class="title">
						Users
					</span>
                    <?php if ($actionUrl=='Users/addUser'||$actionUrl=='Users/addMultipleUsers'||$actionUrl=='Users/index'||$actionUrl=='Users/online') { ?>
                    <span class="selected">
					</span>
                    
                    <?php } ?>
					<span class="arrow <?php if ($actionUrl=='Users/addUser'||$actionUrl=='Users/addMultipleUsers'||$actionUrl=='Users/index'||$actionUrl=='Users/online') { ?>open<?php } ?>">
					</span>
					</a>
					<ul class="dropdown-menu">
                   <?php  if($this->UserAuth->HP('Users', 'addUser')) {
								echo "<li class='".(($actionUrl=='Users/addUser') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Add User'), array('controller'=>'Users', 'action'=>'addUser', 'plugin'=>'usermgmt'))."</li>";
							}
							if($this->UserAuth->HP('Users', 'addMultipleUsers')) {
								echo "<li class='".(($actionUrl=='Users/addMultipleUsers') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Add Multiple Users'), array('controller'=>'Users', 'action'=>'addMultipleUsers', 'plugin'=>'usermgmt'))."</li>";
							}
							if($this->UserAuth->HP('Users', 'index')) {
								echo "<li class='".(($actionUrl=='Users/index') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('All Users'), array('controller'=>'Users', 'action'=>'index', 'plugin'=>'usermgmt'))."</li>";
							}
							if($this->UserAuth->HP('Users', 'online')) {
								echo "<li class='".(($actionUrl=='Users/online') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Online Users'), array('controller'=>'Users', 'action'=>'online', 'plugin'=>'usermgmt'))."</li>";
							} ?>
						
					</ul>
				</li>
                <?php } ?>
                
                
                
                  <?php  
			   
			   //User Groups Menu
			   
			   if($this->UserAuth->HP('UserGroups', 'addGroup')) { ?>
               <li class="dropdown-submenu <?php if ($actionUrl=='UserGroups/addGroup'||$actionUrl=='UserGroups/index') { ?>active <?php } ?>">
					<a href="javascript:;">
					<i class="fa fa-group"></i>
					<span class="title">
						User Groups
					</span>
                  <?php if ($actionUrl=='UserGroups/addGroup'||$actionUrl=='UserGroups/index') { ?>
                    <span class="selected">
					</span>
                    
                    <?php } ?>
					<span class="arrow <?php if ($actionUrl=='UserGroups/addGroup'||$actionUrl=='UserGroups/index') { ?>open<?php } ?>">
					</span>
					</a>
					<ul class="dropdown-menu">
               
               <?php
					
							if($this->UserAuth->HP('UserGroups', 'addGroup')) {
								echo "<li class='".(($actionUrl=='UserGroups/addGroup') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Add Group'), array('controller'=>'UserGroups', 'action'=>'addGroup', 'plugin'=>'usermgmt'))."</li>";
							}
							if($this->UserAuth->HP('UserGroups', 'index')) {
								echo "<li class='".(($actionUrl=='UserGroups/index') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('All Groups'), array('controller'=>'UserGroups', 'action'=>'index', 'plugin'=>'usermgmt'))."</li>";
							}  ?>
                            </ul></li>
                            <?php
			  
			  
			   }?>
            
					
					<?php 	
					
					//Permissions Menu
					
					if($this->UserAuth->HP('UserGroupPermissions', 'index')) { ?>
				 <li class="dropdown-submenu <?php if ($actionUrl=='UserGroupPermissions/index'||$actionUrl=='UserGroupPermissions/subPermissions') { ?>active <?php } ?>">
					<a href="javascript:;">
					<i class="fa fa-unlock"></i>
					<span class="title">
						Permissions
					</span>
                   <?php if ($actionUrl=='UserGroupPermissions/index'||$actionUrl=='UserGroupPermissions/subPermissions') { ?>
                    <span class="selected">
					</span>
                    
                    <?php } ?>
					<span class="arrow <?php if ($actionUrl=='UserGroupPermissions/index'||$actionUrl=='UserGroupPermissions/subPermissions') { ?>open<?php } ?>">
					</span>
					</a>
					<ul class="dropdown-menu">
                   <?php  if($this->UserAuth->HP('UserGroupPermissions', 'index')) {
								echo "<li class='".(($actionUrl=='UserGroupPermissions/index') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Group Permissions'), array('controller'=>'UserGroupPermissions', 'action'=>'index', 'plugin'=>'usermgmt'))."</li>";
							}
							if($this->UserAuth->HP('UserGroupPermissions', 'subPermissions')) {
								echo "<li class='".(($actionUrl=='UserGroupPermissions/subPermissions') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Subgroup Permissions'), array('controller'=>'UserGroupPermissions', 'action'=>'subPermissions', 'plugin'=>'usermgmt'))."</li>";
							}
							?>
						
					</ul>
				</li>
                <?php } ?>
                
             
                
                
                     <?php 
					 
					 //Settings Menu
					 
					 
					 if($this->UserAuth->HP('UserSettings', 'index')) { ?>
                    
                 <li class="dropdown-submenu <?php if ($actionUrl=='UserSettings/index'||$actionUrl=='UserSettings/cakelog') { ?> active <?php } ?>">
					<a href="javascript:;">
					<i class="fa fa-wrench"></i>
					<span class="title">
						Settings
					</span>
					<?php if ($actionUrl=='UserSettings/index'||$actionUrl=='UserSettings/cakelog') { ?><span class="selected">
					</span><?php } ?>
					<span class="arrow <?php if ($actionUrl=='UserSettings/index'||$actionUrl=='UserSettings/cakelog') { ?>open<?php } ?>">
					</span>
					</a>
					<ul class="dropdown-menu">
                    <?php if($this->UserAuth->HP('UserSettings', 'index')) {
								echo "<li class='".(($actionUrl=='UserSettings/index') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('All Settings'), array('controller'=>'UserSettings', 'action'=>'index', 'plugin'=>'usermgmt'))."</li>";
							}
							if($this->UserAuth->HP('UserSettings', 'cakelog')) {
								echo "<li class='".(($actionUrl=='UserSettings/cakelog') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Cake Logs'), array('controller'=>'UserSettings', 'action'=>'cakelog', 'plugin'=>'usermgmt'))."</li>";
							}
							if($this->UserAuth->HP('Users', 'deleteCache')) {
								echo "<li>".$this->Html->link(__('Delete Cache'), array('controller'=>'Users', 'action'=>'deleteCache', 'plugin'=>'usermgmt'))."</li>";
							} ?>
                             </ul>
				</li>
					<?php } ?>
               
              
						
					</ul>
				</li>

                <?php 
} ?>
				
			</ul>

		</div>
		<!-- END HORIZANTAL MENU -->
		<!-- END LOGO -->
		
		<!-- BEGIN TOP NAVIGATION MENU -->
</div>

<div class="collapse navbar-collapse navbar-right">
        <?php if($this->UserAuth->isLogged()) { 
		?>
		<ul class="nav navbar-nav pull-right">
        
			<!-- BEGIN NOTIFICATION DROPDOWN -->
			<li id="sharebutton">
				<a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<button type="button" class="btn btn-sm blue"><i class="fa  fa-share-square-o"></i> Share</button>
				</a>
				<ul class="dropdown-menu extended notification">
					<li><p>Share this Page</p><p>Precreated Tracking Links</p></li>
					<li>
					<?php 

					$url = Router::url( null, true );
					if($this->UserAuth->isLogged()) { 
					$url .= '?shareref='.$var['User']['id'].'';
					}
						echo $url; ?>
					</li>
					
				</ul>
			</li>
			<!-- END NOTIFICATION DROPDOWN -->

			<!-- BEGIN USER LOGIN DROPDOWN -->
			<li class="dropdown user">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<img alt="<?php echo ($var['User']['first_name'].' '.$var['User']['last_name']); ?>'s Avatar" src="<?php echo $this->Image->resize('img/'.IMG_DIR, $var['UserDetail']['photo'], 25, null, true) ?>"/>
				<span class="username">
					<?php echo ($var['User']['first_name'].' '.$var['User']['last_name']); ?>
				</span>
				<i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu">
			  <?php 
				  
								echo "<li class='".(($actionUrl=='Users/myprofile') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('View Profile'), array('controller'=>'Users', 'action'=>'myprofile', 'plugin'=>'usermgmt'))."</li>";
							 if($this->UserAuth->HP('Users', 'editProfile')) {
								echo "<li class='".(($actionUrl=='Users/editProfile') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Edit Profile'), array('controller'=>'Users', 'action'=>'editProfile', 'plugin'=>'usermgmt'))."</li>"; } 
								
								if($this->UserAuth->HP('Users', 'changePassword')) {
								echo "<li class='".(($actionUrl=='Users/changePassword') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Change Password'), array('controller'=>'Users', 'action'=>'changePassword', 'plugin'=>'usermgmt'))."</li>";
							}?>
					
					<?php echo "<li>".$this->Html->link(__('Log Out'), array('controller'=>'Users', 'action'=>'logout', 'plugin'=>'usermgmt'))."</li>";  ?>
				</ul>
			</li>
			<!-- END USER LOGIN DROPDOWN -->
		</ul>
        <?php }
        else {
  ?>
            <ul class="nav navbar-nav login-nav">
                
                <li><a href="/register">Register</a></li>
                <li class="divider-vertical"></li>
                <li class="dropdown sign-in-dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">Sign In <strong class="caret"></strong></a>
                    <div class="dropdown-menu">
                      <?php echo $this->element('toplogin'); ?>
                      <?php echo $this->element('sociallogin'); ?>
                      </div>
                </li>
            </ul>

       <?php  }?>
</div>

    <!-- Global Search Field -->
    <div class="col-sm-2 col-md-3 col-lg-2 pull-right global-search">
        <?php echo $this->Form->create('Search', array('url' => '/searches/prodsearch')) ?>

        <div class="input-group">
            <input type="text" class="form-control" placeholder="Product Search" name="newsearchterm">
            <div class="input-group-btn">
                <button class="btn blue" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
	</div>
	<!-- END TOP NAVIGATION BAR -->
</div>
		
    <section class="ct-content-wrapper">
			<div class="container">
				<?php echo $this->fetch('content'); ?>
			</div>
    </section>
		
		<!--
		<footer>
        
    </footer>
		-->
		<div class="footer">
			<div class="footer-inner">
			Find It For Less
			</div>
			<div class="footer-tools">
				<span class="go-top">
					<i class="fa fa-angle-up"></i>
				</span>
			</div>
		</div>
		
		<?php 
			
			echo $this->Html->script('bootstrap.min.js');
			echo $this->Html->script('icheck.js');
		?>
		<script>
			$('.dropdown-menu input, .dropdown-menu label').click(function(e) {
					e.stopPropagation();
			});
			
			$(document).ready(function(){
			  $('input').iCheck({
			    checkboxClass: 'icheckbox_polaris',
			    radioClass: 'iradio_polaris',
			    increaseArea: '-10%' // optional
			  });
			});
			
			var button = $('#search-btn');
			button.click(function () {
				$('#ct-search-wrapper').toggleClass('show');
			});
			var button = $('#search-collapse');
			button.click(function () {
				
				$('#ct-search-wrapper').toggleClass('show');
			});
			
			$('#tabs a').click(function (e) {
				e.preventDefault()
				$(this).tab('show');
				$('#h4.title').html();
				
			})

			
		</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>