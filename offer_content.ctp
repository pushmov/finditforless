				<div class="container">
						<div class="row">
								<div class="col-sm-3 col-md-3 col-lg-3 sidebar-wrapper">
										
										<ul class="nav nav-tabs" id="tabs">
											<li class="top active"><a href="#top" data-toggle="tab"><i class="fa fa-star white"></i>TOP</a></li>
											<li class="newest"><a href="#newest" data-toggle="tab"><i class="fa fa-angle-double-up white"></i>NEWEST</a></li>
											<li class="discussed"><a href="#discussed" data-toggle="tab"><i class="fa fa-comments white"></i>DISCUSSED</a></li>
										</ul>
										<div class="tab-content">
											<div id="top" class="tab-pane active sidebar-content">
													<div><h5>Top content</h5></div>
													<div><a href="#">Top PS4 Console</a></div>
													<div><a href="#">Fifa 14</a></div>
													<div><a href="#">Nzxt Phantom</a></div>
													<div><a href="#">Nike Blazer</a></div>
													<div><a href="#">Electric Water Cooler</a></div>
													<div><a href="#">Water Cooler</a></div>
													<div><a href="#">New Rechargeable High Quality Bluetooth Wireless Gamepad Controller For Ps3 Red for &pound;12 at eBay UK</a></div>
											</div>
											<div id="newest" class="tab-pane sidebar-content">		
													<div><h5>Newest Tab content</h5></div>
													<div><a href="#">Newest PS4 Console</a></div>
													<div><a href="#">Fifa 14</a></div>
													<div><a href="#">Nzxt Phantom</a></div>
													<div><a href="#">Nike Blazer</a></div>
													<div><a href="#">Electric Water Cooler</a></div>
													<div><a href="#">Water Cooler</a></div>
											</div>
											<div id="discussed" class="tab-pane sidebar-content">		
													<div><h5>Discussed Tab content</h5></div>
													<div><a href="#">Discussed PS4 Console</a></div>
													<div><a href="#">Fifa 14</a></div>
													<div><a href="#">Nzxt Phantom</a></div>
													<div><a href="#">Nike Blazer</a></div>
													<div><a href="#">Electric Water Cooler</a></div>
													<div><a href="#">Water Cooler</a></div>
											</div>
										</div>
										
										<div class="top-search-box">
											<h5><i class="fa fa-star"></i>Today's Top Searches</h5>
											<div class="top-search-box-wrapper">
													<div><a href="#">Discussed PS4 Console</a></div>
													<div><a href="#">Fifa 14</a></div>
													<div><a href="#">Nzxt Phantom</a></div>
													<div><a href="#">Nike Blazer</a></div>
													<div><a href="#">Electric Water Cooler</a></div>
													<div><a href="#">Water Cooler</a></div>
											</div>
										</div>
										
										<div class="top-search-box">
											<h5><i class="fa fa-envelope-o"></i>Newsletter</h5>
											<div class="top-search-box-wrapper">
													<div><a href="#">Discussed PS4 Console</a></div>
													<div><a href="#">Fifa 14</a></div>
													<div><a href="#">Nzxt Phantom</a></div>
													<div><a href="#">Nike Blazer</a></div>
													<div><a href="#">Electric Water Cooler</a></div>
													<div><a href="#">Water Cooler</a></div>
											</div>
										</div>
										
										
								</div>
								<div class="col-sm-9 col-md-9 col-lg-9 content">
										<?php
											$paginator = $this->Paginator;
											foreach ($offers as $item):
										?>
										<div class="row">
												<div class="clearfix item-box">
														<div class="col-sm-3 col-md-3 col-lg-3 photo-item">
															<div class="img-wrapper">
															<?php
																if(strpos($item['Offer']['image'],'http') !== false) {
																	echo $this->Html->image($item['Offer']['image']);
																}
																else {
																	echo $this->Html->image('offers/'.$item['Offer']['image']);
																}
															?>
															</div>
															<div class="btn-photo-group">
																<button class="btn btn-success" onclick="window.location='<?php echo $this->webroot; ?>goto/offers/<?php echo $item['Offer']['id']; ?>'">Go To Offer</button>
																<button class="btn btn-more">More Information</button>
															</div>
														</div>
														<div class="col-sm-7 col-md-7 col-lg-7 description-item">
																<h3><?php echo $this->Html->link($item['Offer']['title'].' for &pound;'.$item['Offer']['price'].' at '.$item['Offer']['merchant'].'', array(
                                    'controller' => 'offers',
                                    'action' => 'view',
                                    'id' => $item['Offer']['id'],
                                    'slug' => $item['Offer']['slug'],


                                ),array('escape' => FALSE)); ?>
																</h3>
																
																<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
														</div>
														<div class="col-sm-2 col-md-2 col-lg-2 rating-item">
																<div class="rating-box">
																	<h1>100</h1>
																	<h5>RATING</h5>
																</div>
																<div class="add-rating-box">
																	<div>
																		<i class="fa fa-thumbs-up fa-2x white"></i>
																	</div>
																	<div><h5>VOTE UP</h5></div>
																</div>
																<div class="rem-rating-box">
																	<div>
																		<i class="fa fa-thumbs-down fa-2x white"></i>
																	</div>
																	<div><h5>VOTE DOWN</h5></div>
																</div>
														</div>
												</div>
												<div class="clearfix content-footer">
													<div class="col-sm-8 col-md-8 col-lg-8 admin-info-box">
															<h5><?php echo $item['User']['username']?> posted <?php echo $this->Time->timeAgoInWords(
																		$item['Offer']['created'],
																		array('format' => 'F jS, Y', 'end' => '+1 year')
																); ?> | <a href="#"><i class="fa fa-comment-o fa-lg"></i>10 comments</a></h5>
													</div>
													<div class="col-sm-4 col-md-4 col-lg-4 admin-btn-control">
															<button class="btn btn-edit"><i class="fa fa-edit white"></i>EDIT</button><button class="btn btn-danger"><i class="fa fa-times white"></i>DELETE</button>
													</div>
												</div>
										</div>
										<?php endforeach; ?>
										
										<ul class="pagination">
											<li><a href="#">&laquo;</a></li>
											<li class="active"><a href="#">1</a></li>
											<li><a href="#">2</a></li>
											<li><a href="#">3</a></li>
											<li><a href="#">4</a></li>
											<li><a href="#">5</a></li>
											<li><a href="#">&raquo;</a></li>
										</ul>
								</div>
						</div>
					
				</div>