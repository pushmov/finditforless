<div class="row clearfix">


    <div class="col-md-3 column">
        <div class="panel panel-default hidden-xs hidden-sm">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <span class="fa fa-search-plus"></span> Product Search</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <?php echo $this->Form->create('Search', array('controller' => 'searches', 'action' => 'prodsearch')); ?>



                    <?php echo $this->Form->input('newsearchterm', array('required' => true, 'class' => 'form-control', 'label' => false, 'placeholder' => 'Enter Product Name', 'name' => 'newsearchterm')); ?>

                </div>
                <?php echo $this->Form->submit('Search', array('class' => 'required btn blue pull-right', 'title' => 'Search Now!')); ?>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
        <div class="panel panel-default hidden-xs hidden-sm">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <span class="glyphicon glyphicon-flash"></span> Related Searches</h3>
            </div>
            <div class="panel-body">
                <ul class="list-group">
                    <?php

                    //Related Searches
                    foreach ($relatedsearches as $relatedsearch) {
                        //var_dump($relatedsearch);
                        $urlword = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $relatedsearch['searches']['searchterm']);
                        $urlword = str_replace(" ", "-", $urlword);
                        $niceword = ucwords($relatedsearch['searches']['searchterm']);?>

                        <li class="list-group-item"><a
                                href="/search/<?php echo $urlword; ?>/"><?php echo $niceword; ?></a></li>

                    <?php
                    }


                    ?>

                </ul>
            </div>

        </div>
        <div class="panel panel-default hidden-xs hidden-sm">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <span class="fa fa-star-o"></span> Today's Top Searches</h3>
            </div>
            <div class="panel-body">
                <ul class="list-group">

                    <?php foreach ($topsearches as $topsearch) {
                        $urlword = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $topsearch['Search']['searchterm']);
                        $urlword = str_replace(" ", "-", $urlword);
                        $niceword = ucwords($topsearch['Search']['searchterm']);

                        ?>
                        <li class="list-group-item"><a
                                href="/search/<?php echo $urlword; ?>/"><?php echo $niceword; ?></a></li>



                    <?php
                    }


                    ?>


                </ul>
            </div>

        </div>
    </div>
    <div class="col-md-9 column">
        <?php
        if ($this->UserAuth->isAdmin()) {
            echo $this->element('edit_searchterm', array('searchterm' => $searchterm));

        }
        ?>



        <?php //var_dump($test); ?>


        <div id="resultbox" class="col-md-12">

        </div>

        <script>
            // JavaScript Document
            var query = '<?php

echo $searchterm; ?>';
            performSearch(query);
            function performSearch(query) {
                maxresults = $('#maxresults').val() || 24;
                page = $('#page').val() || 1;
                price = $('#price').val() || 'asc';
                //store = $('#store').val() || '';
                merchant = $('#merchant').val() || '';

                // Display loading box...
                $('#resultbox').html('<div id="loader"> \
			 <div class="panel panel-default">  <div class="panel-body" align="center"><h3 class="page-title">We are just getting the best prices for "' + query + '"</h3> \
			   <img src="/images/loading.gif" alt="Loading Results" /></div></div></div>');

                // Load content (POST, then retrieve, display..)

                var dataString = 'search=' + query + '&maxresults=' + maxresults + '&page=' + page + '&price=' + price + '&merchant=' + merchant;
                // alert (dataString);
                $.ajax({
                    type: "POST",
                    url: "<?php echo $this->webroot; ?>/files/itemresultsboot.php",
                    data: dataString,
                    success: function (data, textStatus, jqXHR) {
                        $('#resultbox').html(data);
                        // if($('#noitems').text())
                        // $('a[href=#tab2]').click();
                        $('a.updatePage').click(function () {
                            //event.preventDefault();
                            $('#page').val(this.id.replace('page', ''));
                            performSearch(query);
                        });
                    }
                });


                return false;
            }


            var page = {};


        </script>
    </div>