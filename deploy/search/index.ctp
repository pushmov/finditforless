<div class="container">
 <h3 class="page-title">
					Recent Search Queries
					</h3>
<table class="table table-striped table-hover">
								<thead>
								<tr>
									<th>Search Term</th>
									<th>User</th>
									<th>
										Search Date
									</th>
								  </tr>
								</thead>
								<tbody>
								
                                <?php foreach ($searches as $search) { 
								
								
	?>
                                
                                <tr>
									<td>
                                    <?php 
									$searchterm=strtolower($search['Search']['searchterm']);
                                    $searchterm = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $searchterm);
                                    $searchterm=str_replace(" ","-",$searchterm);
									
									 ?>
										<a href="/search/<?php echo $searchterm; ?>/" target="_blank"/><?php echo $search['Search']['searchterm']; ?></a>
									</td>
									<td>
										<?php
										if (!$search['User']['username']==''){
										
										 echo $search['User']['username']; }
										 else
										 {
											 echo 'Guest';
											 
										 }?>
									</td>
									<td>
										<?php echo $this->Time->format(
  'F jS, Y h:i A',
  $search['Search']['created'],
  null,
  'Europe/London'
);

 ?>
									</td>
								  </tr> <?php } ?>
								</tbody>
								</table></td>
                                </div>