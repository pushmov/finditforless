<?php

class SearchesController extends AppController
{

    public function find()
    {

        //Get the Variable from the URL
        $searchterm = str_replace('search/', '', $this->params['pass']['0']);
        $searchterm = str_replace('/', '', $searchterm);
        $searchterm = str_replace('-', ' ', $searchterm);
        $searchterm = strtolower($searchterm);
        $this->set('searchterm', $searchterm);


        //Create Page Title

        $title = ucwords($searchterm);
        $this->set('title_for_layout', $title);

        //Create Meta Description
        $this->set('description_for_layout', 'Compare prices for ' . $title . ' online instantly at thousands of stores. Guaranteed to find you the best deal for ' . $title . ' online within 2 minutes!');

        //Create Meta Keywords
        $keywords = '' . $searchterm . ' price comparison, lowest prices for ' . $searchterm . ', discount ' . $searchterm . ', ' . $searchterm . ' promotion and special offers, cheap ' . $searchterm . ' online';
        $this->set('keywords_for_layout', $keywords);


        //Add to Database with User ID
        if ($this->UserAuth->isLogged()) {
            //Check if search has happened within the last 10 seconds, just to prevent duplicates with some browser plugins reloading quickly
            $checksearchexists = $this->Search->find('count', array(
                'conditions' => array('Search.searchterm' => $searchterm, 'Search.created >' => date('Y-m-d H:i:s', strtotime('-10 seconds')), 'Search.user_id' => $this->Session->read('UserAuth.User.id'))
            ));
            //If no searches within 10 seconds, add it
            if ($checksearchexists == '0') {
                $data = array('searchterm' => $searchterm, 'user_id' => $this->Session->read('UserAuth.User.id'));
                $this->Search->save($data);
            }
        } //Add to Database as Guest
        else if (!$this->UserAuth->isLogged()) {

            //Check if search has happened within the last 10 seconds, just to prevent duplicates with some browser plugins reloading quickly
            $checksearchexists = $this->Search->find('count', array(
                'conditions' => array('Search.searchterm' => $searchterm, 'Search.created >' => date('Y-m-d H:i:s', strtotime('-10 seconds')), 'Search.user_id' => '0')
            ));
            //If no searches within 10 seconds, add it
            if ($checksearchexists == '0') {
                $data = array('searchterm' => $searchterm);
                $this->Search->save($data);
            }
        }

        //Top Searches

        $topsearches = $this->Search->find('all',
            array('conditions' => array(
                'Search.created >' => date('Y-m-d H:i:s', strtotime('-1 day'))),
                'fields' => array('Search.searchterm', 'count(Search.searchterm) as cnt'),
                'order' => array('cnt DESC'),
                'group' => array('Search.searchterm'),
                'limit' => 10,
            )
        );
        $this->set('topsearches', $topsearches);

        //Related Searches


        $query = "SELECT * FROM searches WHERE";

        $tags = explode(" ", $searchterm);
        foreach ($tags as $words) {
            $number = strlen($words);
            if (strlen($words) >= 3) {


                $query .= " searchterm REGEXP '[[:<:]]{$words}[[:>:]]' OR";


            }


        }

        $query .= " GROUP BY searchterm LIMIT 10";
        $query = str_replace('OR GROUP BY', 'GROUP BY', $query);
        $related = $this->Search->query("$query");
        $this->set('relatedsearches', $related);

    }


    public function index()
    {
        $this->set('title_for_layout', 'User Search History');
        $this->paginate = array('limit' => 50, 'order' => 'Search.created desc');
        $searches = $this->paginate('Search');
        $this->set('searches', $searches);


    }

//This processes the search field, and redirects to URL
    public function prodsearch()
    {

        if ($this->request->is('post')) {
            $this->Search->set($this->request->data);
        }

        $search = strtolower($this->request->data['newsearchterm']);

        $search = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $search);
        $search = str_replace(" ", "-", $search);

//$this->redirect(''.Router::fullbaseUrl().''.$this->webroot.'search/'.$search.'/');
        $this->redirect('' . Router::fullbaseUrl() . '' . $this->webroot . 'search/' . $search . '/');

    }


}

?>