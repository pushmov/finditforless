<?php
/**
 * Enter description here ...
 * @author hhso
 */
class OffersController extends AppController {
    var $uses = array('Offer', 'Category');
    var $components = array('Upload','Paginator');





    function view($id = null, $slug = null) {

        $viewoffer = $this->Offer->find('first', array(
            'conditions' => array('Offer.id' => $id)
        ));
        $this->set('viewoffer', $viewoffer);

        //Create Page Title

        $title = ''.$viewoffer['Offer']['title'].' for £'.$viewoffer['Offer']['price'].' at '.$viewoffer['Offer']['merchant'].'';
        $this->set('title_for_layout',$title);

        //Create Meta Description
        $this->set('description_for_layout','Take a look at '.$viewoffer['Offer']['title'].' for only £'.$viewoffer['Offer']['price'].' at '.$viewoffer['Offer']['merchant'].'. ');

        //Create Meta Keywords
        $keywords = ''.$viewoffer['Offer']['title'].', '.$viewoffer['Offer']['merchant'].','.$viewoffer['Offer']['title'].' on offer for £'.$viewoffer['Offer']['price'].'';
        $this->set('keywords_for_layout',$keywords);
    }


    function index() {
       // $offers = $this->Offer->find('all', array());
        //$this->set(compact('offers'));
			$this->layout = 'offerLayout';
			$this->paginate = array(
				'limit' => 15,
			);
			$offers = $this->paginate('Offer');
    
			// pass the value to our view.ctp
			$this->set('offers', $offers);
			// $this->render('offerContent');
    }

    function bycategory($catslug = null) {

				$this->layout = 'offerLayout';

        $this->paginate = array(
            'limit' => 15,
        );



        $offers = $this->paginate('Offer');


        $this->set('offers', $offers);
        //$this->set('catslug', $catslug);

        $this -> render('/Offers/index');
    }

    
	public function add() {
        $this->set('title_for_layout','Add Offer');
		$postData = $this->data;
        $destination = DS.'img'.DS.'files'.DS;
	    $allowedExt = array('png', 'jpg', 'gif');
	    $maxSize = 10485760 /*10MB*/;
	    
	  
	  
		 $this->set('cats', $this->Category->find('list'));
		 
        if ($this->request->is('post')) {
			$slug = Inflector::slug(strtolower($this->data['Offer']['title']), '-');
			$this->request->data['Offer']['slug'] = $slug;
			$offerimage = $this->data['Offer']['image_url'];
			
			if ($this->__validate($postData)) {
                // process uploaded file
                if (!empty($postData['Offer']['file'])) {
                    $proccessedItem = $this->Upload->uploadFile($postData['Offer']['file'], $destination, $allowedExt, $maxSize, time());
        	        //resize image
        	        if (in_array($proccessedItem['fileExt'], array('png', 'jpg', 'gif', 'bmp'))) {
        	            $this->Upload->resizeImage($proccessedItem['absolutePath']);
        	        }
        	        if ($proccessedItem) {
        	             $offerimage = $proccessedItem['fileName']; 
        	        }
                } 
				elseif (empty($postData['Offer']['file']) && $this->data['Offer']['image_url'] != '') {
				
				$offerimage = $this->data['Offer']['image_url']; } 
				 
				 }
                
               //$this->request->data['Offer']['image'] = $slug;
			   
			 $this->request->data['Offer']['image'] = $offerimage;
            $this->Offer->create();
            if ($this->Offer->save($this->request->data)) {
                $this->Session->setFlash(__('Your post has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('Unable to add your offer.'));
        }
    }
    /**
     * @author hhso
     * Enter description here ...
     */
    function edit($id) {
        $postData = $this->data;
        $destination = DS.'img'.DS.'files'.DS;
	    $allowedExt = array('png', 'jpg', 'gif');
	    $maxSize = 10485760 /*10MB*/;
	    
	    $this->set('cats', $this->Category->find('list'));
	    
        if (!empty($postData)) {
            //debug($postData);exit;
            
            if ($this->__validate($postData)) {
                // process uploaded file
                if (!empty($postData['Offer']['file'])) {
                    $proccessedItem = $this->Upload->uploadFile($postData['Offer']['file'], $destination, $allowedExt, $maxSize, time());
        	        //resize image
        	        if (in_array($proccessedItem['fileExt'], array('png', 'jpg', 'gif', 'bmp'))) {
        	            $this->Upload->resizeImage($proccessedItem['absolutePath']);
        	        }
        	        if ($proccessedItem) {
        	            $postData['Offer']['image'] = $proccessedItem['fileName']; 
        	        }
                } elseif (empty($postData['Offer']['file']) && $postData['Offer']['image_url'] != '') $postData['Offer']['image'] = $postData['Offer']['image_url'];
                
                if (!$this->Offer->save($postData)) {
                    foreach($this->Offer->validationErrors as $col => $err) {
                        if ($err[0] != '') $this->Session->setFlash($col.' '.$err[0]);break;    
                    }
                } else {
                    $this->Session->setFlash('Offer is saved!');
                    $id = $this->Offer->id;
                    $saveOffer = $this->Offer->findById($id);
                    $this->set('offer', $saveOffer['Offer']);
                    $this->redirect("/Offers/edit/$id");
                }
            }
        }
        if (!isset($id) || $id==0) {
            $offer['Offer'] = $this->__initOffer();
        } else $offer = $this->Offer->findById($id);
        $this->set('offer', $offer['Offer']);
    }
    
    /**
     * @author hhso
     * Enter description here ...
     */
    function __initOffer() {
        return array(
            'id'         => '',
            'link'       => '',
            'title'      => '',
            'merchant'   => '',
            'price'      => 0,
            'cat_id'     => '',
            'desc'       => '',
            'status'     => '',
            'email'      => '',
            'image'      => '',
            'image_url'  => '',
            'tag'        => '',
            'start_date' => '',
            'end_date'   => ''
        );
    }
    
    /**
     * @author hhso
     * Enter description here ...
     * @param Array $postData
     */
    function __validate($postData) {
        // put extra validate here
        return true;
    }
	
	public function addfromsearch() {

        //Display the default details to populate the form
		    $this->set('cats', $this->Category->find('list'));
$itemid = $this->params['pass']['0'];
require_once('/var/www/vhosts/finditforless.co.uk/beta/pf/sphinxapi.php');
$s = new SphinxClient;
$s->setServer("localhost", 9312);
$s->SetMatchMode(SPH_MATCH_EXTENDED);
$results = $s->Query('@prodid '.$itemid.'', 'aw, cj, td, ebay, amazon');
    foreach ($results['matches'] as $item) {
    $this->set('itemdetails', $item);


 }

        //Add the offer
        if ($this->request->is('post')) {
            $slug = Inflector::slug(strtolower($this->data['Offer']['title']), '-');
            $this->request->data['Offer']['slug'] = $slug;

            $this->request->data['Offer']['image'] =  $offerimage = $this->data['Offer']['image_url'];
            $this->Offer->create();
            if ($this->Offer->save($this->request->data)) {
                $this->Session->setFlash(__('<h3>Offer Added</h3><p>You have added <a href="/'.$slug.'-'.$this->Offer->getLastInsertId().'/">'.$this->data['Offer']['title'].'</a></p>'));
                return $this->redirect($this->referer());
            }
            $this->Session->setFlash(__('Unable to add your offer.'));
        }
        }


    public function delete($id) {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        if ($this->Offer->delete($id)) {
            $this->Session->setFlash(
                __('Offer Deleted.', h($id))
            );
            return $this->redirect(array('action' => 'index'));
        }
    }
}
