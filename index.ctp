<div class="row clearfix">


<div class="col-md-3 column">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li><a href="#top" data-toggle="tab">Top</a></li>
        <li><a href="#newest" data-toggle="tab">Newest</a></li>
        <li><a href="#discussed" data-toggle="tab">Discussed</a></li>

    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="top">Top</div>
        <div class="tab-pane" id="newest">Newest</div>
        <div class="tab-pane" id="discussed">Discussed</div>

    </div>
    <div class="panel panel-default hidden-xs hidden-sm">
        <div class="panel-heading">
            <h3 class="panel-title">
                <span class="fa fa-star-o"></span> Today's Top Searches</h3>
        </div>
        <div class="panel-body">

        </div>

    </div>
    <div class="panel panel-default hidden-xs hidden-sm">
        <div class="panel-heading">
            <h3 class="panel-title">
                <span class="fa fa-star-o"></span> Newsletter</h3>
        </div>
        <div class="panel-body">

        </div>

    </div>
</div>
<div class="col-md-9 column">


<?php
        $paginator = $this->Paginator;

        foreach ($offers as $item):
            ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row clearfix">


                        <div class="col-md-2 column centered">

                            <?php


                            if (strpos($item['Offer']['image'],'http') !== false) {
                                echo  $this->Html->image($item['Offer']['image'], array('class' => "img-thumbnail center-block"));
                            }

                            else {

                                echo  $this->Html->image('offers/'.$item['Offer']['image'], array('class' => "img-thumbnail center-block"));
                            }


                            ?>

                            <?php

                            echo $this->Html->link(__('<i class="fa fa-share"></i> View Offer', true), array(
                                'controller' => 'goto',
                                'action' => 'offers',
                                'id' => $item['Offer']['id']),
                                array('class' => 'btn btn-lg green center-block','escape' => false));



                         ?>



                        </div>
                        <div class="col-md-10column">
                            <h4><?php echo $this->Html->link(''.$item['Offer']['title'].' for £'.$item['Offer']['price'].' at '.$item['Offer']['merchant'].'', array(
                                    'controller' => 'offers',
                                    'action' => 'view',
                                    'id' => $item['Offer']['id'],
                                    'slug' => $item['Offer']['slug'],


                                ),array('escape' => FALSE)); ?></h4>
                            Content
                            </div>
                     </div>
                </div>
                <div class="panel-footer clearfix">
                    <?php echo $item['User']['username']?> posted <?php echo $this->Time->timeAgoInWords(
                            $item['Offer']['created'],
                            array('format' => 'F jS, Y', 'end' => '+1 year')
                        ); ?>
                    <?php
                    echo $this->Form->postLink(
                        'Delete',

                        array('action' => 'delete', $item['Offer']['id']),
                        array('class' => 'btn default btn-xs red pull-right'),
                        array('confirm' => 'Are you sure you want to delete '.$item['Offer']['title'].'?')

                    );
                    ?>
                </div>
            </div>

    <?php endforeach; ?>  <?php

    // pagination section
    echo "<div class='paging'>";

    // the 'first' page button
    echo $paginator->first("First");

    // 'prev' page button,
    // we can check using the paginator hasPrev() method if there's a previous page
    // save with the 'next' page button
    if($paginator->hasPrev()){
        echo $paginator->prev("Prev");
    }

    // the 'number' page buttons
    echo $paginator->numbers(array('modulus' => 2));

    // for the 'next' button
    if($paginator->hasNext()){
        echo $paginator->next("Next");
    }

    // the 'last' page button
    echo $paginator->last("Last");

    echo "</div>";

    ?>

</div>



