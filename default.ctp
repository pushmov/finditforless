<?php
$contName = Inflector::camelize($this->params['controller']);
$actName = $this->params['action'];
$actionUrl = $contName.'/'.$actName;
$activeClass='active';
$inactiveClass='';


	

?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <?php echo $this->Html->charset(); ?>
<title><?php echo $title_for_layout; ?> | Find It For Less</title>
    <? if(isset($description_for_layout)){ ?>
<meta name = "description" content="<?=$description_for_layout;?>" />
    <? } ?>
    <? if(isset($keywords_for_layout)){ ?>
<meta name = "keywords" content="<?=$keywords_for_layout;?>" />
    <? } ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="Find It For Less" name="author"/>
<meta name="MobileOptimized" content="320">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="/fifl/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/fifl/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/fifl/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
<link href="/fifl/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="/fifl/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="/fifl/css/style-metronic.css" rel="stylesheet" type="text/css"/>
<link href="/fifl/css/style.css" rel="stylesheet" type="text/css"/>
<link href="/fifl/css/finditforless.css" rel="stylesheet" type="text/css"/>
<link href="/fifl/css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="/fifl/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="/fifl/css/themes/fifl.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="/fifl/css/custom.css" rel="stylesheet" type="text/css"/>

<?php

		
		/* Usermgmt Plugin CSS */
		echo $this->Html->css('/usermgmt/css/umstyle.css?q='.QRDN);
		
		/* Bootstrap Datepicker is taken from https://github.com/eternicode/bootstrap-datepicker */
		echo $this->Html->css('/plugins/bootstrap-datepicker/css/datepicker.css?q='.QRDN);

		/* Bootstrap Datepicker is taken from https://github.com/smalot/bootstrap-datetimepicker */
		echo $this->Html->css('/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css?q='.QRDN);
		
		/* Chosen is taken from https://github.com/harvesthq/chosen/releases/tag/0.14.0 */
		echo $this->Html->css('/plugins/chosen/chosen.css?q='.QRDN);

		/* Jquery latest version taken from http://jquery.com */
		echo $this->Html->script('jquery-1.10.2.min.js');
		
		/* Bootstrap JS */
		echo $this->Html->script('bootstrap.js?q='.QRDN);

		/* Bootstrap Datepicker is taken from https://github.com/eternicode/bootstrap-datepicker */
		echo $this->Html->script('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js?q='.QRDN);

		/* Bootstrap Datepicker is taken from https://github.com/smalot/bootstrap-datetimepicker */
		echo $this->Html->script('/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js?q='.QRDN);
		
		/* Bootstrap Typeahead is taken from https://github.com/biggora/bootstrap-ajax-typeahead */
		echo $this->Html->script('/plugins/bootstrap-ajax-typeahead/js/bootstrap-typeahead.min.js?q='.QRDN);
		
		/* Chosen is taken from https://github.com/harvesthq/chosen/releases/tag/0.14.0 */
		echo $this->Html->script('/plugins/chosen/chosen.jquery.min.js?q='.QRDN);

		/* Usermgmt Plugin JS */
		echo $this->Html->script('/usermgmt/js/umscript.js?q='.QRDN);
		echo $this->Html->script('/usermgmt/js/ajaxValidation.js?q='.QRDN);

		echo $this->Html->script('/usermgmt/js/chosen/chosen.ajaxaddition.jquery.js?q='.QRDN);

	?>

<!-- END THEME STYLES -->

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-full-width">

<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse navbar-fixed-top">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="header-inner">
		<!-- BEGIN LOGO -->
		<a class="navbar-brand" href="index.html">
		<img src="/fifl/img/logo.png" alt="logo" class="img-responsive"/>
		</a>
        	<div class="hor-menu hidden-sm hidden-xs">
			<ul class="nav navbar-nav">
				<li>
					<a href="index.html">
					Dashboard </a>
				</li>
<li>
					<a data-toggle="dropdown" data-hover="dropdown" data-close-others="true" class="dropdown-toggle" href=""><i class="fa fa-gbp"></i> Offers <i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu">

<li><?php echo $this->Html->link('Top Offers', array(
                                    'controller' => 'offers',
                                    'action' => 'bycategory',
'plugin' => NULL,
                                    'catslug' => 'top'
                                                                   )); ?></li>
<li><?php echo $this->Html->link('Latest Offers', array(
                                    'controller' => 'offers',
                                    'action' => 'bycategory',
'plugin' => NULL,
                                    'catslug' => 'newest'
                                                                   )); ?></li>

<li><?php echo $this->Html->link('Popular Offers', array(
                                    'controller' => 'offers',
                                    'action' => 'bycategory',
'plugin' => NULL,
                                    'catslug' => 'popular'

                                                                   )); ?></li>								

<li class="divider"></li>
 <li role="presentation" class="dropdown-header">Categories</li>
<?php foreach($menucats as $menucat): ?>

			
		

<li><?php echo $this->Html->link(''.$menucat['Category']['name'].'', array(
                                    'controller' => 'offers',
                                    'action' => 'bycategory',
'plugin' => NULL,
                                    'catslug' => $menucat['Category']['slug']
                                                                   )); ?></li>

 <?php endforeach; ?> 

					</ul>


                <?php 
if ($this->UserAuth->isAdmin()) { ?>
				<li>
					<a data-toggle="dropdown" data-hover="dropdown" data-close-others="true" class="dropdown-toggle" href=""><i class="fa fa-cogs"></i> Admin <i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu">
						 <?php  
			   
			   //Searches Menu
			   
			   if($this->UserAuth->HP('Searches', 'index')) { ?>
               <li  class="dropdown-submenu <?php if ($actionUrl=='Searches/index'||$actionUrl=='UserGroups/index') { ?> active<?php } ?>">
					<a href="javascript:;">
					<i class="fa fa-search"></i>
					<span class="title">
						Searches
					</span>
                  <?php if ($actionUrl=='UserGroups/addGroup'||$actionUrl=='UserGroups/index') { ?>
                    <span class="selected">
					</span>
                    
                    <?php } ?>
					<span class="arrow <?php if ($actionUrl=='UserGroups/addGroup'||$actionUrl=='UserGroups/index') { ?>open<?php } ?>">
					</span>
					</a>
					<ul class="dropdown-menu">
               
               <?php
					
							if($this->UserAuth->HP('Searches', 'index')) {
								echo "<li class='".(($actionUrl=='Searches/index') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('View Searches'), array('controller'=>'Searches', 'action'=>'index', 'plugin'=>null))."</li>";
							}
							if($this->UserAuth->HP('UserGroups', 'index')) {
								echo "<li class='".(($actionUrl=='UserGroups/index') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('All Groups'), array('controller'=>'UserGroups', 'action'=>'index', 'plugin'=>'usermgmt'))."</li>";
							}  ?>
                            <li></ul>
                            <?php
			  
			  
			   }?>
               <?php  
			   
			   //Transactions Menu
			   
			   if($this->UserAuth->HP('UserGroups', 'addGroup')) { ?>
               <li class="dropdown-submenu <?php if ($actionUrl=='UserGroups/addGroup'||$actionUrl=='UserGroups/index') { ?>active <?php } ?>">
					<a href="javascript:;">
					<i class="fa fa-truck"></i>
					<span class="title">
						Transactions
					</span>
                  <?php if ($actionUrl=='UserGroups/addGroup'||$actionUrl=='UserGroups/index') { ?>
                    <span class="selected">
					</span>
                    
                    <?php } ?>
					<span class="arrow <?php if ($actionUrl=='UserGroups/addGroup'||$actionUrl=='UserGroups/index') { ?>open<?php } ?>">
					</span>
					</a>
					<ul class="dropdown-menu">
               
               <?php
					
							if($this->UserAuth->HP('UserGroups', 'addGroup')) {
								echo "<li class='".(($actionUrl=='UserGroups/addGroup') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Add Group'), array('controller'=>'UserGroups', 'action'=>'addGroup', 'plugin'=>'usermgmt'))."</li>";
							}
							if($this->UserAuth->HP('UserGroups', 'index')) {
								echo "<li class='".(($actionUrl=='UserGroups/index') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('All Groups'), array('controller'=>'UserGroups', 'action'=>'index', 'plugin'=>'usermgmt'))."</li>";
							}  ?>
                            <li></ul>
                            <?php
			  
			  
			   }?>
                     <?php
					   
					   //Categories Menu
					   
					    if($this->UserAuth->HP('Categories', 'add')) { ?>
                    
                 <li class="dropdown-submenu <?php if ($actionUrl=='Categories/add'||$actionUrl=='Categories/index') { ?> active <?php } ?>">
					<a href="javascript:;">
					<i class="fa fa-folder-open-o"></i>
					<span class="title">
						Categories
					</span>
					<?php if ($actionUrl=='Categories/add'||$actionUrl=='Categories/index') { ?><span class="selected">
					</span><?php } ?>
					<span class="arrow <?php if ($actionUrl=='Categories/add'||$actionUrl=='Categories/index') { ?>open<?php } ?>">
					</a>
					<ul class="dropdown-menu">
                <?php
            
				
							if($this->UserAuth->HP('Categories', 'add')) {
								echo "<li class='".(($actionUrl=='Categories/add') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Add Category'), array('controller'=>'Categories', 'action'=>'add', 'plugin'=>null))."</li>";
							}
							
							if($this->UserAuth->HP('Users', 'index')) {
								echo "<li class='".(($actionUrl=='Categories/index') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Edit Categories'), array('controller'=>'Categories', 'action'=>'index','plugin'=>null))."</li>";
							}	 ?>
                            
                            </ul>
				</li>
                <?php					
					}
					?> 
					 <?php 	
				
				//Users Menu
				
				
				if($this->UserAuth->HP('Users', 'addUser')) { ?>
				<li class="dropdown-submenu <?php if ($actionUrl=='Users/addUser'||$actionUrl=='Users/addMultipleUsers'||$actionUrl=='Users/index'||$actionUrl=='Users/online') { ?> active <?php } ?>">
					<a href="javascript:;">
					<i class="fa fa-user"></i>
					<span class="title">
						Users
					</span>
                    <?php if ($actionUrl=='Users/addUser'||$actionUrl=='Users/addMultipleUsers'||$actionUrl=='Users/index'||$actionUrl=='Users/online') { ?>
                    <span class="selected">
					</span>
                    
                    <?php } ?>
					<span class="arrow <?php if ($actionUrl=='Users/addUser'||$actionUrl=='Users/addMultipleUsers'||$actionUrl=='Users/index'||$actionUrl=='Users/online') { ?>open<?php } ?>">
					</span>
					</a>
					<ul class="dropdown-menu">
                   <?php  if($this->UserAuth->HP('Users', 'addUser')) {
								echo "<li class='".(($actionUrl=='Users/addUser') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Add User'), array('controller'=>'Users', 'action'=>'addUser', 'plugin'=>'usermgmt'))."</li>";
							}
							if($this->UserAuth->HP('Users', 'addMultipleUsers')) {
								echo "<li class='".(($actionUrl=='Users/addMultipleUsers') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Add Multiple Users'), array('controller'=>'Users', 'action'=>'addMultipleUsers', 'plugin'=>'usermgmt'))."</li>";
							}
							if($this->UserAuth->HP('Users', 'index')) {
								echo "<li class='".(($actionUrl=='Users/index') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('All Users'), array('controller'=>'Users', 'action'=>'index', 'plugin'=>'usermgmt'))."</li>";
							}
							if($this->UserAuth->HP('Users', 'online')) {
								echo "<li class='".(($actionUrl=='Users/online') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Online Users'), array('controller'=>'Users', 'action'=>'online', 'plugin'=>'usermgmt'))."</li>";
							} ?>
						
					</ul>
				</li>
                <?php } ?>
                
                
                
                  <?php  
			   
			   //User Groups Menu
			   
			   if($this->UserAuth->HP('UserGroups', 'addGroup')) { ?>
               <li class="dropdown-submenu <?php if ($actionUrl=='UserGroups/addGroup'||$actionUrl=='UserGroups/index') { ?>active <?php } ?>">
					<a href="javascript:;">
					<i class="fa fa-group"></i>
					<span class="title">
						User Groups
					</span>
                  <?php if ($actionUrl=='UserGroups/addGroup'||$actionUrl=='UserGroups/index') { ?>
                    <span class="selected">
					</span>
                    
                    <?php } ?>
					<span class="arrow <?php if ($actionUrl=='UserGroups/addGroup'||$actionUrl=='UserGroups/index') { ?>open<?php } ?>">
					</span>
					</a>
					<ul class="dropdown-menu">
               
               <?php
					
							if($this->UserAuth->HP('UserGroups', 'addGroup')) {
								echo "<li class='".(($actionUrl=='UserGroups/addGroup') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Add Group'), array('controller'=>'UserGroups', 'action'=>'addGroup', 'plugin'=>'usermgmt'))."</li>";
							}
							if($this->UserAuth->HP('UserGroups', 'index')) {
								echo "<li class='".(($actionUrl=='UserGroups/index') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('All Groups'), array('controller'=>'UserGroups', 'action'=>'index', 'plugin'=>'usermgmt'))."</li>";
							}  ?>
                            <li></ul>
                            <?php
			  
			  
			   }?>
            
					
					<?php 	
					
					//Permissions Menu
					
					if($this->UserAuth->HP('UserGroupPermissions', 'index')) { ?>
				 <li class="dropdown-submenu <?php if ($actionUrl=='UserGroupPermissions/index'||$actionUrl=='UserGroupPermissions/subPermissions') { ?>active <?php } ?>">
					<a href="javascript:;">
					<i class="fa fa-unlock"></i>
					<span class="title">
						Permissions
					</span>
                   <?php if ($actionUrl=='UserGroupPermissions/index'||$actionUrl=='UserGroupPermissions/subPermissions') { ?>
                    <span class="selected">
					</span>
                    
                    <?php } ?>
					<span class="arrow <?php if ($actionUrl=='UserGroupPermissions/index'||$actionUrl=='UserGroupPermissions/subPermissions') { ?>open<?php } ?>">
					</span>
					</a>
					<ul class="dropdown-menu">
                   <?php  if($this->UserAuth->HP('UserGroupPermissions', 'index')) {
								echo "<li class='".(($actionUrl=='UserGroupPermissions/index') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Group Permissions'), array('controller'=>'UserGroupPermissions', 'action'=>'index', 'plugin'=>'usermgmt'))."</li>";
							}
							if($this->UserAuth->HP('UserGroupPermissions', 'subPermissions')) {
								echo "<li class='".(($actionUrl=='UserGroupPermissions/subPermissions') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Subgroup Permissions'), array('controller'=>'UserGroupPermissions', 'action'=>'subPermissions', 'plugin'=>'usermgmt'))."</li>";
							}
							?>
						
					</ul>
				</li>
                <?php } ?>
                
             
                
                
                     <?php 
					 
					 //Settings Menu
					 
					 
					 if($this->UserAuth->HP('UserSettings', 'index')) { ?>
                    
                 <li class="dropdown-submenu <?php if ($actionUrl=='UserSettings/index'||$actionUrl=='UserSettings/cakelog') { ?> active <?php } ?>">
					<a href="javascript:;">
					<i class="fa fa-wrench"></i>
					<span class="title">
						Settings
					</span>
					<?php if ($actionUrl=='UserSettings/index'||$actionUrl=='UserSettings/cakelog') { ?><span class="selected">
					</span><?php } ?>
					<span class="arrow <?php if ($actionUrl=='UserSettings/index'||$actionUrl=='UserSettings/cakelog') { ?>open<?php } ?>">
					</a>
					<ul class="dropdown-menu">
                    <?php if($this->UserAuth->HP('UserSettings', 'index')) {
								echo "<li class='".(($actionUrl=='UserSettings/index') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('All Settings'), array('controller'=>'UserSettings', 'action'=>'index', 'plugin'=>'usermgmt'))."</li>";
							}
							if($this->UserAuth->HP('UserSettings', 'cakelog')) {
								echo "<li class='".(($actionUrl=='UserSettings/cakelog') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Cake Logs'), array('controller'=>'UserSettings', 'action'=>'cakelog', 'plugin'=>'usermgmt'))."</li>";
							}
							if($this->UserAuth->HP('Users', 'deleteCache')) {
								echo "<li>".$this->Html->link(__('Delete Cache'), array('controller'=>'Users', 'action'=>'deleteCache', 'plugin'=>'usermgmt'))."</li>";
							} ?>
                             </ul>
				</li>
					<?php } ?>
               
              
						
					</ul>
				</li>

                <?php 
} ?>
				
			</ul>

		</div>
		<!-- END HORIZANTAL MENU -->
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<img src="/fifl/img/menu-toggler.png" alt=""/>
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->




                <?php echo $this->Form->end(); ?>
</div>
        <?php if($this->UserAuth->isLogged()) { 
		?>
		<ul class="nav navbar-nav pull-right">
        <li>


        </li>
			<!-- BEGIN NOTIFICATION DROPDOWN -->
			<li id="sharebutton">
				<a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<button type="button" class="btn btn-sm blue"><i class="fa  fa-share-square-o"></i> Share</button>
</a>
				<ul class="dropdown-menu extended notification">
					<li>
						<p>
							Share this Page
						</p>

<p>
							Precreated Tracking Links
						</p>


					</li>
					<li>
					<?php 

					$url = Router::url( null, true );
					if($this->UserAuth->isLogged()) { 
					$url .= '?shareref='.$var['User']['id'].'';
					}
						echo $url; ?>
					</li>
					
				</ul>
			</li>
			<!-- END NOTIFICATION DROPDOWN -->

			<!-- BEGIN USER LOGIN DROPDOWN -->
			<li class="dropdown user">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<img alt="<?php echo ($var['User']['first_name'].' '.$var['User']['last_name']); ?>'s Avatar" src="<?php echo $this->Image->resize('img/'.IMG_DIR, $var['UserDetail']['photo'], 25, null, true) ?>"/>
				<span class="username">
					<?php echo ($var['User']['first_name'].' '.$var['User']['last_name']); ?>
				</span>
				<i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu">
			  <?php 
				  
								echo "<li class='".(($actionUrl=='Users/myprofile') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('View Profile'), array('controller'=>'Users', 'action'=>'myprofile', 'plugin'=>'usermgmt'))."</li>";
							 if($this->UserAuth->HP('Users', 'editProfile')) {
								echo "<li class='".(($actionUrl=='Users/editProfile') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Edit Profile'), array('controller'=>'Users', 'action'=>'editProfile', 'plugin'=>'usermgmt'))."</li>"; } 
								
								if($this->UserAuth->HP('Users', 'changePassword')) {
								echo "<li class='".(($actionUrl=='Users/changePassword') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Change Password'), array('controller'=>'Users', 'action'=>'changePassword', 'plugin'=>'usermgmt'))."</li>";
							}?>
					
                        
                      
					
					
					<?php echo "<li>".$this->Html->link(__('Log Out'), array('controller'=>'Users', 'action'=>'logout', 'plugin'=>'usermgmt'))."</li>";  ?>
				</ul>
			</li>
			<!-- END USER LOGIN DROPDOWN -->
		</ul>
        <?php }
        else {
  ?>
            <ul class="nav navbar-nav pull-right">
                <li>

                </li>
                <li><a href="/register">Register</a></li>
                <li class="divider-vertical"></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">Sign In <strong class="caret"></strong></a>
                    <div class="dropdown-menu">
                      <?php echo $this->element('toplogin'); ?>
                      <?php echo $this->element('sociallogin'); ?>
                      </div>
                </li>
            </ul>







       <?php  }?>


    <!-- Global Search Field -->
    <div class="col-sm-2 col-md-3 col-lg-2 pull-right">
        <?php echo $this->Form->create('Search', array('url' => '/searches/prodsearch')) ?>

        <div class="input-group">
            <input type="text" class="form-control" placeholder="Product Search" name="newsearchterm">
            <div class="input-group-btn">
                <button class="btn blue" type="submit"><i class="glyphicon glyphicon-search white"></i></button>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
	</div>
	<!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
        
			
            
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
			

			<?php //echo $this->element('Usermgmt.message_validation'); ?>
			<?php echo $this->fetch('content'); ?>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<div class="footer-inner">
	Find It For Less
	</div>
	<div class="footer-tools">
		<span class="go-top">
			<i class="fa fa-angle-up"></i>
		</span>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
   <script src="/fifl/plugins/respond.min.js"></script>
   <script src="/fifl/plugins/excanvas.min.js"></script> 
<script src="/fifl/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="/fifl/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<!--<script src="/fifl/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>-->
<script src="/fifl/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/fifl/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/fifl/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/fifl/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/fifl/scripts/jquery.gritter.js"></script>
<script src="/fifl/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->
<script src="/fifl/scripts/ui-general.js"></script>
<script src="/fifl/scripts/app.js"></script>
<script type="text/javascript" src="/fifl/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/fifl/scripts/form-validation.js"></script>
<script src="/fifl/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="/fifl/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/fifl/scripts/app.js"></script>
<script src="/fifl/scripts/ui-extended-modals.js"></script>

<script>

console.log('here');
      jQuery(document).ready(function() {    
         App.init();
		  FormValidation.init();
		   UIExtendedModals.init();
      });
   </script>
<?php
//Get the User's group ID
$groupid = $this->UserAuth->getGroupId();
//Show the Feedback bar // On;y for admins and beta testers
if ($this->UserAuth->isAdmin() || $groupid == '4') {
echo $this->element('FeedbackIt.feedbackbar');
} ?>
<?php

echo $this->element('Usermgmt.message'); ?>


<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>